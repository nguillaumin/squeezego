# Completion of the Slim protocol implementation

Legend: ⚪ = not applicable, 🟢 = fully implemented, 🟠 = partially implemented, 🔴 = not implemented

## Client to Server

  - ⚪ ALSS: Not implemented in the server
  - ⚪ ANIC: Irrelevant as bitmap graphics are not supported (yet?)
  - 🔴 BODY
  - ⚪ BUTN: Redundant with IR
  - 🟢 BYE
  - ⚪ DBUG: Unnecessary?
  - 🟠 DSCO
  - 🟠 HELO
  - 🟢 IR
  - ⚪ KNOB Irrelevant without physical knobs
  - 🔴 META
  - ⚪ RAWI: Redundant with IR
  - 🔴 RESP
  - 🔴 SETD: Could be useful to change the display width as a SqueezeSlave client
  - 🟠 STAT
  - ⚪ UREQ: Irrelevant

## Server to Client

- ⚪ audc: Irrelevant (?), no clock source
- 🔴 aude
- ⚪ audf: Irrelevant, Transporter effect loop specific
- 🔴 audg: Correctly parsed but does not do anything yet
- ⚪ audo: Irrelevant, Boom subwoofer specific
- ⚪ audr: Irrelevant, Transporter specific
- ⚪ bdac: Irrelevant, Boom DAC specific
- 🔴 body
- ⚪ brir: Irrelevant, Boom brightness specific
- 🔴 cont
- ⚪ grfb: Probably irrelevant? (Revisit once LCD is implemented)
- ⚪ grfd: Irrelevant, Squeezebox 1 graphics specific
- ⚪ grfe: Irrelevant as bitmap graphics are not supported (yet?)
- ⚪ i2cc: Irrelevant, for Squeezebox 1
- ⚪ knoa: Irrelevant, knob handling
- ⚪ knob: Irrelevant, Transporter knob handling
- ⚪ rsps: Irrelevant, Transporter RS-232 specific
- ⚪ rstx: Irrelevant, Transporter RS-232 specific
- ⚪ setd: Probably irrelevant, firmware settings
- 🟠 strm
- ⚪ test: Irrelevant, for testing plugins
- ⚪ upda: Irrelevant, firmware upgrade
- ⚪ updn: Irrelevant, firmware upgrade
- ⚪ ureq: Irrelevant, firmware upgrade
- 🟢 vers
- 🟢 vfdc
- ⚪ visu: Irrelevant as bitmap graphics are not supported (yet?)