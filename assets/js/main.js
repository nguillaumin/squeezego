document.addEventListener("DOMContentLoaded", function (event) {
  var parts = window.location.href.split("/")
  var ws = new WebSocket(`ws://${parts[2]}/ws`);

  ws.onopen = () => {
    var areas = document.getElementsByTagName("area");
    for (var i = 0; i < areas.length; i++) {
      var area = areas.item(i);
      area.onclick = evt => {
        ws.send(
          JSON.stringify({
            type: "ir",
            message: {
              code: Number(evt.target.id),
            },
          })
        );
        return false;
      };
    }
  };

  ws.onmessage = event => {
    var message = JSON.parse(event.data);
    switch (message.type) {
      case "vfdc":
        document.getElementById("vfdc").value = message.message.data;
    }
  };

  ws.onclose = event => {
    alert("Websocket connection closed");
  };
});
