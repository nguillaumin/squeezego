.PHONY: all test build

all: test build

generate:
	cd build/assets && go generate -tags dev && cd -

test: generate
	go test -cover ./...

build: generate
	go build cmd/squeezego/squeezego.go

clean:
	go clean
	rm -f squeezego

docker:
	docker build build/docker/logitech-media-server/ -t squeezego

lms:
	docker run -p 9000:9000 -p 3483:3483 squeezego
