// +build ignore

package main

import (
	"log"

	"github.com/shurcooL/vfsgen"
	"gitlab.com/nguillaumin/squeezego/build/assets"
)

func main() {
	err := vfsgen.Generate(assets.Assets, vfsgen.Options{
		PackageName:  "assets",
		BuildTags:    "!dev",
		VariableName: "Assets",
	})

	if err != nil {
		log.Fatal(err)
	}
}
