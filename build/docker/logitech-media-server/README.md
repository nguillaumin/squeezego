# Logitech Media Server testing image

This folder contains a Docker image for Logitech Media Server, already
configured and containing a few audio file samples in different formats.
Logging has been configured to be more verbose around networking and protocols.

This is intended to be used in manual testing with SqueezeGo.

To use, first build the image:

```
docker build . -t squeezego
```

Then run it with:

```
docker run -p 9000:9000 -p 3483:3483 squeezego
```

Then run SqueezeGo with the `-server localhost` flag.

Connect to the web interface at http://localhost:9000/
