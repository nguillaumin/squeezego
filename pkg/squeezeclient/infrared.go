package squeezeclient

import (
	"bufio"
	"io"
	"net"
	"os"
	"strings"

	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"
)

const lircdSocket = "/var/run/lirc/lircd"

var irCodes = map[string]uint16{
	"KEY_UP":           0xe01f,
	"KEY_DOWN":         0xb04f,
	"KEY_RIGHT":        0xd02f,
	"KEY_LEFT":         0x906f,
	"KEY_PREVIOUSSONG": 0xc03f,
	"KEY_NEXTSONG":     0xa05f,
	"KEY_PLAYPAUSE":    0x10ef,
	"KEY_STOPCD":       0x20df,
	"KEY_HOMEPAGE":     0x22dd,
	"KEY_MUTE":         0xc43b,
	"KEY_VOLUMEDOWN":   0x00ff,
	"KEY_VOLUMEUP":     0x807f,
	"KEY_PAGEUP":       0xe01f,
	"KEY_PAGEDOWN":     0xb04f,
}

func (client *SqueezeClient) lircSocketLoop() {
	_, err := os.Stat(lircdSocket)
	if os.IsNotExist(err) {
		log.GetLog("client.infrared").Infof("lircd socket %v not found, disabling infrared support", lircdSocket)
		return
	}

	conn, err := net.Dial("unix", lircdSocket)
	if err != nil {
		log.GetLog("client.infrared").Warnf("Unable to read lircd socket %v: %v", lircdSocket, err)
		return
	}
	defer conn.Close()

	reader := bufio.NewReader(conn)
	for {
		str, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				return
			} else {
				log.GetLog("client.infrared").Warnf("Error reading from lirc socket: %v", err)
			}
		}
		parts := strings.Split(str, " ")
		if len(parts) != 4 {
			log.GetLog("client.infrared").Warnf("Invalid message from lirc socket. %v parts received, 4 expected", len(parts))
		} else {
			log.GetLog("client.infrared").Debugf("Received command %v from lirc socket", parts[2])
			if code, ok := irCodes[parts[2]]; ok {
				client.sendIR(code)
			}
		}
	}
}

func (client *SqueezeClient) sendIR(irCode uint16) error {
	ir := &slimprotoclient.IrCommand{
		client.ticks,
		0,
		0,
		irCode,
	}
	return client.SendCommand(ir)
}
