package squeezeclient

import (
	"bufio"
	"io"
	"net"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"
)

func (client *SqueezeClient) telnetControlInitialize() {
	listener, err := net.Listen("tcp", ":8181")
	if err != nil {
		log.GetLog("telnet").Errorf("Error setting up socket: %v", err)
		return
	}

	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.GetLog("client").Errorf("Error accepting remote connection: %v", err)
			}
			client.clientListeners = append(client.clientListeners, conn)
			log.GetLog("telnet").WithFields(logrus.Fields{
				"client": conn.RemoteAddr,
			}).Debugf("Accepted client connection. %v clients so far", len(client.clientListeners))
			go client.handleTelnetControlCommand(conn)
		}
	}()
}

func (client *SqueezeClient) handleTelnetControlCommand(conn net.Conn) {
	reader := bufio.NewReader(conn)
	for {
		str, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				// FIXME: Remove from the list of clients in client.clientListeners
				return
			}
			log.GetLog("telnet").WithFields(logrus.Fields{
				"client": conn.RemoteAddr,
			}).Warnf("Error reading command from client: %v", err)
		}
		str = strings.TrimRight(str, "\r\n")
		value, err := strconv.ParseUint(str, 16, 16)
		if err != nil {
			log.GetLog("telnet").WithFields(logrus.Fields{
				"client":        conn.RemoteAddr,
				"commandString": str,
			}).Warnf("Error parsing command from client: %v", err)
		}
		client.sendIR(uint16(value))
	}
}
