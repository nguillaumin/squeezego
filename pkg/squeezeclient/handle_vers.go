package squeezeclient

import (
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoserver"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"

	"github.com/sirupsen/logrus"
)

func (client *SqueezeClient) HandleVers(commandName string, commandData []byte) (slimprotoclient.ClientCommand, error) {
	vers := slimprotoserver.VersCommand{}
	err := vers.UnmarshalBinary(commandData)

	if err != nil {
		return nil, err
	}

	log.GetLog("client.command").WithFields(logrus.Fields{
		"serverVersion": vers.Version,
	}).Info("Server version")

	return nil, nil
}
