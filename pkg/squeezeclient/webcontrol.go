package squeezeclient

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"

	"gitlab.com/nguillaumin/squeezego/build/assets"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"
)

var upgrader = websocket.Upgrader{}

// Envelope wraps a websocket message
type Envelope struct {
	Type    string                 `json:"type"`
	Message map[string]interface{} `json:"message"`
}

// WebSocketHandler handles websocket connection
type WebSocketHandler struct {
	client *SqueezeClient
	conns  []*websocket.Conn
}

func (client *SqueezeClient) webControlInitialize() {
	if client.webControlPort > 0 {
		client.websocketHandler = WebSocketHandler{
			client: client,
		}

		fs := http.FileServer(assets.Assets)
		http.Handle("/", fs)
		http.Handle("/ws", &client.websocketHandler)

		go http.ListenAndServe(fmt.Sprintf(":%v", client.webControlPort), nil)
	}
}

func (handler *WebSocketHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.GetLog("client.webcontrol.websocket").Errorf("error upgrading HTTP connection: %v", err)
		return
	}

	handler.conns = append(handler.conns, conn)

	for err == nil {
		envelope := Envelope{}
		err = conn.ReadJSON(&envelope)

		if err == nil {
			log.GetLog("Client.webcontrol.websocket").Debugf("Received message %v", envelope)

			switch envelope.Type {
			case "ir":
				handler.client.sendIR(uint16(envelope.Message["code"].(float64)))
			}
		}
	}

	log.GetLog("client.webcontrol.websocket").Errorf("error reading WS message: %v", err)
}
