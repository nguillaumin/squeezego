package decoder

import (
	"bytes"
	"encoding/binary"
	"io"
	"reflect"

	"github.com/hajimehoshi/go-mp3"
)

// Mp3Decoder decodes MP3 audio streams
//
// Only limited formats are supported (MPEG-1 only)
type Mp3Decoder struct {
	decoder mp3.Decoder
	buffer  []int16
}

// NewMp3Decoder returns a new MP3 decoder
func NewMp3Decoder(in io.Reader) (*Mp3Decoder, error) {
	decoder, err := mp3.NewDecoder(in)
	if err != nil {
		return nil, err
	}

	return &Mp3Decoder{
		decoder: *decoder,
	}, nil
}

// ReadFloat32 is not implemented for the Mp3Decoder
func (decoder *Mp3Decoder) ReadFloat32(out []float32) (int, error) {
	panic("Not implemented")
}

// ReadInt16 decodes int16 samples into the output buffer
func (decoder *Mp3Decoder) ReadInt16(out []int16) (int, error) {
	var err error
	// Output are 16bit samples, but the decoder read bytes,
	// so we need twice the size
	buf := make([]byte, len(out)*2)
	var n int

	// Read enough to fill the output buffer
	for len(decoder.buffer) < len(out) && err == nil {
		n, err = decoder.decoder.Read(buf)

		if err != nil && err != io.EOF {
			return n, err
		}

		if n > 0 {
			// Decode bytes into int16
			decoded := make([]int16, n/2)
			bufReader := bytes.NewReader(buf[0:n])
			errDecode := binary.Read(bufReader, binary.LittleEndian, &decoded)

			if errDecode != nil {
				return n, errDecode
			}
			decoder.buffer = append(decoder.buffer, decoded...)
		}
	}

	// Fill the output buffer
	n = copy(out, decoder.buffer)

	// Truncate internal buffer to what was read
	decoder.buffer = decoder.buffer[n:]

	if err == io.EOF && len(decoder.buffer) <= 0 {
		return n, io.EOF
	}

	return n, nil
}

// GetSampleRate returns the sample rate of the MP3 stream
func (decoder *Mp3Decoder) GetSampleRate() float64 {
	return float64(decoder.decoder.SampleRate())
}

// GetChannels returns the number of channels of the MP3 stream
//
// In practice always 2 as the underlying library always outputs
// 2 channels. See https://github.com/hajimehoshi/go-mp3/pull/20
func (decoder *Mp3Decoder) GetChannels() int {
	return 2
}

// GetSampleType returns the type of samples used by the Mp3Decoder
func (decoder *Mp3Decoder) GetSampleType() reflect.Kind {
	return reflect.Int16
}

// Close is a no-op for the Mp3Decoder
func (decoder *Mp3Decoder) Close() {

}
