package decoder

import (
	"io"
	"reflect"

	"github.com/jfreymuth/oggvorbis"
)

// OggDecoder decodes Ogg audio streams
type OggDecoder struct {
	oggReader *oggvorbis.Reader
	buffer    []float32
}

// NewOggDecoder returns a new Ogg decoder
func NewOggDecoder(in io.Reader) (*OggDecoder, error) {
	oggReader, err := oggvorbis.NewReader(in)
	if err != nil {
		return nil, err
	}

	return &OggDecoder{
		oggReader: oggReader,
	}, nil
}

// ReadFloat32 decodes float32 samples into the output buffer
func (decoder *OggDecoder) ReadFloat32(out []float32) (int, error) {
	var err error
	buf := make([]float32, len(out))
	var n int

	// Read enough to fill the output buffer
	for len(decoder.buffer) < len(out) && err == nil {
		n, err = decoder.oggReader.Read(buf)

		if err != nil && err != io.EOF {
			return n, err
		}

		if n > 0 {
			decoder.buffer = append(decoder.buffer, buf[0:n]...)
		}
	}

	// Fill the output buffer
	n = copy(out, decoder.buffer)

	// Truncate internal buffer to what was read
	decoder.buffer = decoder.buffer[n:]

	if err == io.EOF && len(decoder.buffer) <= 0 {
		return n, io.EOF
	}

	return n, nil
}

// ReadInt16 is not implemented for the OggDecoder
func (decoder *OggDecoder) ReadInt16(out []int16) (int, error) {
	panic("Not implemented")
}

// GetSampleRate returns the sample rate of the Ogg stream
func (decoder *OggDecoder) GetSampleRate() float64 {
	return float64(decoder.oggReader.SampleRate())
}

// GetChannels returns the number of channels of the Ogg stream
func (decoder *OggDecoder) GetChannels() int {
	return decoder.oggReader.Channels()
}

// GetSampleType returns the type of samples used by the OggDecoder
func (decoder *OggDecoder) GetSampleType() reflect.Kind {
	return reflect.Float32
}

// Close is a no-op for the OggDecoder
func (decoder *OggDecoder) Close() {

}
