package decoder

import (
	"io"
	"reflect"

	"github.com/mewkiz/flac"
	flacframe "github.com/mewkiz/flac/frame"
)

// FlacDecoder decodes Flac audio streams
type FlacDecoder struct {
	stream *flac.Stream
	buffer []int16
}

// NewFlacDecoder returns a new Flac decoder
func NewFlacDecoder(in io.Reader) (*FlacDecoder, error) {
	stream, err := flac.Parse(in)
	if err != nil {
		return nil, err
	}

	return &FlacDecoder{
		stream: stream,
	}, nil
}

// ReadFloat32 is not implemented for the FlacDecoder
func (decoder *FlacDecoder) ReadFloat32(out []float32) (int, error) {
	panic("Not implemented")
}

// ReadInt16 decodes int16 samples into the output buffer
func (decoder *FlacDecoder) ReadInt16(out []int16) (int, error) {
	var err error
	var frame *flacframe.Frame
	var subFrame *flacframe.Subframe

	// Read enough to fill the output buffer
	for len(decoder.buffer) < len(out) && err == nil {
		frame, err = decoder.stream.ParseNext()

		if err != nil && err != io.EOF {
			return 0, err
		}

		if err == nil {
			for i := 0; i < frame.Subframes[0].NSamples; i++ {
				for _, subFrame = range frame.Subframes {
					// FIXME: We assume 16bit FLAC here...
					decoder.buffer = append(decoder.buffer, int16(subFrame.Samples[i]))
				}
			}
		}
	}

	// Fill the output buffer
	n := copy(out, decoder.buffer)

	// Truncate internal buffer to what was read
	decoder.buffer = decoder.buffer[n:]

	if err == io.EOF && len(decoder.buffer) <= 0 {
		return n, io.EOF
	}

	return n, nil
}

// GetSampleRate returns the sample rate of the Flac stream
func (decoder *FlacDecoder) GetSampleRate() float64 {
	return float64(decoder.stream.Info.SampleRate)
}

// GetChannels returns the number of channels of the Flac stream
func (decoder *FlacDecoder) GetChannels() int {
	return int(decoder.stream.Info.NChannels)
}

// GetSampleType returns the type of samples used by the FlacDecoder
func (decoder *FlacDecoder) GetSampleType() reflect.Kind {
	return reflect.Int16
}

// Close closes the underlying Flac stream
func (decoder *FlacDecoder) Close() {
	decoder.stream.Close()
}
