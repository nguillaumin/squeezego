package decoder

import (
	"fmt"
	"io"
	"reflect"
)

// Decoder decodes a stream of audio data
type Decoder interface {
	ReadFloat32(data []float32) (int, error)
	ReadInt16(data []int16) (int, error)
	Close()
	GetSampleRate() float64
	GetChannels() int
	GetSampleType() reflect.Kind
}

// GetDecoder returns a decoder appropriate for the content type of a stream
func GetDecoder(contentType string, reader io.Reader) (Decoder, error) {
	switch contentType {
	case "audio/x-ogg":
		return NewOggDecoder(reader)
	case "audio/x-flac":
		return NewFlacDecoder(reader)
	case "audio/mpeg":
		return NewMp3Decoder(reader)
	}

	return nil, fmt.Errorf("No decoder for content type: %v", contentType)
}
