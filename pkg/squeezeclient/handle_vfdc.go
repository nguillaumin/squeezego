package squeezeclient

import (
	"bufio"
	"strings"

	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoserver"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"

	"github.com/sirupsen/logrus"
)

func (client *SqueezeClient) HandleVfdc(commandName string, commandData []byte) (slimprotoclient.ClientCommand, error) {
	vfdc := slimprotoserver.VfdcCommand{}
	err := vfdc.UnmarshalBinary(commandData)

	if err != nil {
		return nil, err
	}

	buf := make([]byte, 1)

	for i := 0; i < len(commandData); i++ {
		if commandData[i] != 3 || i >= len(commandData) {
			continue
		} else {
			if len(buf) == 41 {
				buf = append(buf, '\n')
			}
			buf = append(buf, commandData[i+1])
		}
	}

	log.GetLog("client.command").WithFields(logrus.Fields{
		"command": commandName,
		"display": string(buf),
	}).Debug("vfdc")

	// Send text to connected telnet clients
	for _, conn := range client.clientListeners {
		writer := bufio.NewWriter(conn)
		// Clear screen
		writer.Write([]byte{0x1b, '[', '2', 'J'})
		// Skip line
		writer.Write([]byte{'\n'})
		writer.WriteString(string(buf))
		writer.Flush()
	}

	// Sanitize string for web control interface
	str := strings.ReplaceAll(string(buf), "\u0000", "")
	str = strings.ReplaceAll(str, "\u0010", "")
	for _, conn := range client.websocketHandler.conns {
		conn.WriteJSON(Envelope{
			Type: "vfdc",
			Message: map[string]interface{}{
				"data": str,
			},
		})
	}

	return nil, nil
}
