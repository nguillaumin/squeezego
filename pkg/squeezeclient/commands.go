package squeezeclient

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto"
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"
)

func (client *SqueezeClient) SendCommand(cmd slimprotoclient.ClientCommand) error {
	b, err := cmd.MarshalBinary()
	if err != nil {
		log.GetLog("client").WithFields(logrus.Fields{
			"commandName": cmd.Name(),
			"command":     cmd,
		}).Error("Error marshalling client command to binary", err)
		return err
	}

	log.GetLog("client.network").Logger.WithFields(logrus.Fields{
		"command": cmd,
	}).Tracef("Sending client command")

	_, err = client.conn.Write(b)
	if err != nil {
		log.GetLog("client.network").Errorf("Failed to send command '%v': %v", cmd.Name(), err)
		return err
	}

	log.GetLog("client.slimproto").WithFields(logrus.Fields{
		"commandName": cmd.Name(),
		"command":     cmd,
	}).Infof("Sent command")

	return nil
}

func (client *SqueezeClient) sendHELO() error {
	helo := &slimprotoclient.HeloCommand{
		slimprotoclient.DeviceIDSqueezeSlave,
		255,
		slimproto.MacAddress{0, 0, 0, 0, 0},
		uuid.Nil,
		0,
		0,
		0,
		"ogg,flc,HasDigitalOut=1",
	}
	return client.SendCommand(helo)
}

func (client *SqueezeClient) sendBYE() error {
	bye := &slimprotoclient.ByeCommand{}
	return client.SendCommand(bye)
}

func (client *SqueezeClient) newStatCommand() *slimprotoclient.StatCommand {
	stat := &slimprotoclient.StatCommand{}
	stat.Jiffies = uint32(time.Now().UnixNano() / int64(time.Millisecond))
	stat.BufferSize = audioBufferSize
	stat.BufferFullness = uint32(client.readCount)
	stat.BytesReceived = client.bytesReceived
	if client.playing {
		stat.ElapsedMilliseconds = uint32(time.Since(client.readStartTime) / time.Millisecond)
		stat.ElapsedSeconds = uint32(time.Since(client.readStartTime) / time.Second)
	}
	return stat
}
