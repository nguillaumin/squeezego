package log

import "github.com/sirupsen/logrus"

func GetLog(module string) *logrus.Entry {
	return logrus.WithField("module", module)
}
