package log

import (
	"github.com/sirupsen/logrus"
)

type FilteringFormatter struct {
	ParentFormatter logrus.Formatter
	Levels          map[string]logrus.Level
}

func (formatter *FilteringFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	if module, ok := entry.Data["module"]; ok {
		if level, ok := formatter.Levels[module.(string)]; ok {
			if entry.Level > level {
				return []byte{}, nil
			}
		}
	}

	return formatter.ParentFormatter.Format(entry)
}
