package squeezeclient

import (
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoserver"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"

	"strings"

	"github.com/sirupsen/logrus"
)

func (client *SqueezeClient) HandleStrm(commandName string, commandData []byte) error {
	strm := &slimprotoserver.StrmCommand{}
	err := strm.UnmarshalBinary(commandData)

	if err != nil {
		return err
	}

	log.GetLog("client.command").WithFields(logrus.Fields{
		"command": strm,
	}).Debug("Received strm command")

	stat := client.newStatCommand()

	switch strm.Command {
	case slimprotoserver.StrmCommandStop:
		client.playing = false
		stat.EventCode = slimprotoclient.StatCodeFlush
		client.SendCommand(stat)
		break
	case slimprotoserver.StrmCommandStart:
		stat.EventCode = slimprotoclient.StatCodeConnect
		httpRequestParts := strings.Split(strm.HTTPRequest, " ")
		client.playStream(strm.ServerIP, strm.ServerPort, httpRequestParts[1])
		break
	case slimprotoserver.StrmCommandStatus:
		stat.EventCode = slimprotoclient.StatCodeTimer
		stat.ServerTimestamp = strm.ReplayGainAsTimestamp()
		client.SendCommand(stat)
		break
	default:
		log.GetLog("client.command").WithField("strmCommand", strm.Command).Warn("Unhandled strm command")
		// Standard stat / info response
		stat.EventCode = slimprotoclient.StatCodeTimer
		client.SendCommand(stat)
		break
	}

	return nil
}
