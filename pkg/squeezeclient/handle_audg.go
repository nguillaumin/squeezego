package squeezeclient

import (
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"
	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoserver"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"

	"github.com/sirupsen/logrus"
)

func (client *SqueezeClient) HandleAudg(commandName string, commandData []byte) (slimprotoclient.ClientCommand, error) {
	audg := slimprotoserver.AudgCommand{}
	err := audg.UnmarshalBinary(commandData)

	if err != nil {
		return nil, err
	}

	log.GetLog("client.command").WithFields(logrus.Fields{
		"command": audg.String(),
	}).Info("Received audg command")

	return nil, nil
}
