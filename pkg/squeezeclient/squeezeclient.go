package squeezeclient

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"net"
	"strings"
	"time"

	"github.com/gordonklaus/portaudio"

	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoclient"

	"github.com/sirupsen/logrus"

	"gitlab.com/nguillaumin/squeezego/pkg/slimproto/slimprotoserver"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/decoder"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"

	"reflect"

	"net/http"
)

const audioBufferSize = 1024 * 8
const connectionAttemptInterval = 10 * time.Second

const (
	statusIdle = iota
	statusPlaying
	statusConnected
)

// SqueezeClient is a client for a SqueezeServer
type SqueezeClient struct {
	host             string
	port             uint16
	audioDevice      *portaudio.DeviceInfo
	webControlPort   uint16
	conn             net.Conn
	status           byte
	playerChannel    chan bool
	readCount        int
	readStartTime    time.Time
	bytesReceived    uint64
	ticker           *time.Ticker
	ticks            uint32
	clientListeners  []net.Conn
	websocketHandler WebSocketHandler
	playing          bool
	closing          bool
}

// NewSqueezeClient returns a new SqueezeClient
func NewSqueezeClient(host string, port uint16, audioDevice *portaudio.DeviceInfo, webControlPort uint16) *SqueezeClient {
	return &SqueezeClient{
		host:           host,
		port:           port,
		webControlPort: webControlPort,
		audioDevice:    audioDevice,
		playerChannel:  make(chan bool),
	}
}

func (client *SqueezeClient) Connect() {
	serverNetwork := fmt.Sprintf("%s:%d", client.host, client.port)
	var err error = errors.New("Initial attempt")

	for err != nil {
		log.GetLog("client.network").Debugf("Attempting to connect to %v...", serverNetwork)
		client.conn, err = net.Dial("tcp", serverNetwork)
		if err != nil {
			log.GetLog("client.network").Warnf("Error connecting to %v: %v. Waiting %v.", serverNetwork, err, connectionAttemptInterval)
			time.Sleep(connectionAttemptInterval)
		}
	}

	log.GetLog("client.network").Debugf("Connected to %v: %v", serverNetwork, client.conn.LocalAddr())

	if client.ticker != nil {
		client.ticker.Stop()
	}

	client.ticks = 0
	client.ticker = time.NewTicker(time.Millisecond)
	go func() {
		for {
			select {
			case <-client.ticker.C:
				client.ticks += 1
			}
		}
	}()
}

func (client *SqueezeClient) Close() error {
	defer client.conn.Close()

	client.closing = true
	err := client.sendBYE()
	if err != nil {
		return err
	}

	return nil
}

func (client *SqueezeClient) Start() error {
	go client.lircSocketLoop()
	client.webControlInitialize()
	client.telnetControlInitialize()

	isNetError := true
	var err error
	for isNetError && !client.closing {
		log.GetLog("client").Info("Starting client loop")
		err = client.clientLoop()
		_, isNetError = err.(net.Error)
		isNetError = isNetError || (err == io.EOF)
		if isNetError && !client.closing {
			log.GetLog("client.network").Warnf("Network error (%v). Attempt to reconnect", err)
			client.conn.Close()
			client.Connect()
		} else {
			log.GetLog("client").Error("Unknown error", err)
		}
	}

	return err
}

func (client *SqueezeClient) clientLoop() error {

	var err error
	err = client.sendHELO()
	if err != nil {
		return err
	}

	for {
		commmandLengthAndData := make([]byte, slimprotoserver.ServerCommandDataLengthByteSize+slimprotoserver.ServerCommandNameLengthByteSize)

		client.conn.SetReadDeadline(time.Now().Add(5 * time.Second))
		_, err = io.ReadFull(client.conn, commmandLengthAndData)
		if err != nil {
			log.GetLog("client.slimproto").Errorf("Error reading command length & name from server: %v", err)
			break
		}

		commandLength := binary.BigEndian.Uint16(commmandLengthAndData[0:2]) - slimprotoserver.ServerCommandNameLengthByteSize
		commandName := string(commmandLengthAndData[2:6])

		log.GetLog("client.slimproto").WithFields(logrus.Fields{
			"command": commandName,
			"length":  commandLength,
		}).Debugf("Read command")

		commandData := make([]byte, commandLength)
		client.conn.SetReadDeadline(time.Now().Add(5 * time.Second))
		_, err = io.ReadFull(client.conn, commandData)
		if err != nil {
			log.GetLog("client").WithFields(logrus.Fields{
				"command": commandName,
				"length":  commandLength,
			}).Error("Error reading command data from server", err)
			break
		}

		methodName := "Handle" + strings.Title(commandName)
		method := reflect.ValueOf(client).MethodByName(methodName)
		if method.IsValid() {
			returnValues := method.Call([]reflect.Value{
				reflect.ValueOf(commandName), reflect.ValueOf(commandData),
			})
			errValue := returnValues[0]

			if !errValue.IsNil() {
				log.GetLog("client").WithFields(logrus.Fields{
					"command": commandName,
					"method":  methodName,
				}).Error("Error handling command", errValue.Interface().(error))
			}
		} else {
			log.GetLog("client").WithFields(logrus.Fields{
				"command":     commandName,
				"commandData": commandData,
				"method":      methodName,
			}).Warn("No method found to handle command")
		}
	}

	return err
}

func (client *SqueezeClient) playStream(serverIp net.IP, serverPort uint16, streamPath string) {
	log.GetLog("client").WithFields(logrus.Fields{
		"streamPath": streamPath,
	}).Infof("Starting stream play")

	streamURL := fmt.Sprintf("http://%s:%d%s", client.host, serverPort, streamPath)
	if !serverIp.Equal(net.IPv4(0, 0, 0, 0)) {
		streamURL = fmt.Sprintf("http://%s:%d%s", serverIp, serverPort, streamPath)
	}

	client.readStartTime = time.Now()
	client.bytesReceived = 0

	stat := client.newStatCommand()
	stat.EventCode = slimprotoclient.StatCodeConnect
	client.SendCommand(stat)

	go func() {
		log.GetLog("client").WithField("streamURL", streamURL).Debug("Requesting stream")
		resp, err := http.Get(streamURL)
		if err != nil {
			return
		}

		defer resp.Body.Close()

		log.GetLog("client").WithField("httpHeaders", resp.Header).Debug("Stream headers")

		decoder, err := decoder.GetDecoder(resp.Header.Get("Content-Type"), resp.Body)
		if err != nil {
			log.GetLog("client").Errorf("Error getting decoder for stream: %v", err)
			stat = client.newStatCommand()
			stat.EventCode = slimprotoclient.StatCodeDecoderError
			client.SendCommand(stat)
			return
		}
		defer decoder.Close()

		streamParameters := portaudio.StreamParameters{
			Output: portaudio.StreamDeviceParameters{
				Device:   client.audioDevice,
				Channels: decoder.GetChannels(),
				Latency:  client.audioDevice.DefaultHighOutputLatency,
			},
			Input: portaudio.StreamDeviceParameters{
				Device: nil,
			},
			SampleRate:      decoder.GetSampleRate(),
			FramesPerBuffer: audioBufferSize,
		}

		log.GetLog("client.audio").WithFields(logrus.Fields{
			"channels":   streamParameters.Output.Channels,
			"sampleRate": streamParameters.SampleRate,
			"sampleType": decoder.GetSampleType(),
		}).Infof("Stream metadata")

		var outFloat32 []float32
		var outInt16 []int16
		var stream *portaudio.Stream

		switch decoder.GetSampleType() {
		case reflect.Float32:
			outFloat32 = make([]float32, audioBufferSize)
			stream, err = portaudio.OpenStream(streamParameters, &outFloat32)
		case reflect.Int16:
			outInt16 = make([]int16, audioBufferSize)
			stream, err = portaudio.OpenStream(streamParameters, &outInt16)
		default:
			panic(fmt.Sprintf("Decoder sample type %v not supported", decoder.GetSampleType()))
		}
		if err != nil {
			log.GetLog("client.audio").Fatal("Unable to open PortAudio stream: ", err)
			return
		}

		defer stream.Close()

		stat = client.newStatCommand()
		stat.EventCode = slimprotoclient.StatCodeStarted
		client.SendCommand(stat)

		err = stream.Start()
		if err != nil {
			log.GetLog("client.audio").Fatalf("Unable to open PortAudio stream: %v", err)
		}
		defer stream.Stop()

		client.playing = true
		for client.playing {
			var n int
			switch decoder.GetSampleType() {
			case reflect.Float32:
				n, err = decoder.ReadFloat32(outFloat32)
			case reflect.Int16:
				n, err = decoder.ReadInt16(outInt16)
			}

			if err != nil && err != io.EOF {
				log.GetLog("client.audio").Debugf("Error decoding audio: %v", err)
				break
			}

			client.readCount += n
			client.bytesReceived += uint64(n)

			writeErr := stream.Write()
			if writeErr != nil {
				log.GetLog("client.audio").Errorf("Error writing to PortAudio: %v", writeErr)
				break
			}

			if err != nil && err == io.EOF {
				log.GetLog("client").WithField("streamURL", streamURL).Debug("Completed stream read")

				client.SendCommand(&slimprotoclient.DscoCommand{})

				stat = client.newStatCommand()
				stat.EventCode = slimprotoclient.StatCodeDecoderReady
				client.SendCommand(stat)

				stat = client.newStatCommand()
				stat.EventCode = slimprotoclient.StatCodeUnderrun
				client.SendCommand(stat)

				return
			}
		}
	}()
}
