package slimprotoclient

import (
	"bytes"
	"encoding/binary"
)

const statCommandName = "STAT"
const statCommandSize = 53

const (
	StatCodePrefix          = "STM"
	StatCodeTimer           = StatCodePrefix + "t"
	StatCodeFlush           = StatCodePrefix + "f"
	StatCodeConnect         = StatCodePrefix + "c"
	StatCodeStarted         = StatCodePrefix + "s"
	StatCodeEstablished     = StatCodePrefix + "e"
	StatCodeHeadersReceived = StatCodePrefix + "h"
	StatCodeUnderrun        = StatCodePrefix + "u"
	StatCodeDecoderReady    = StatCodePrefix + "d"
	StatCodeDecoderError    = StatCodePrefix + "n"
)

type StatCommand struct {
	EventCode            string
	NumCrLf              uint8
	MasInitialized       uint8
	MasMode              uint8
	BufferSize           uint32
	BufferFullness       uint32
	BytesReceived        uint64
	SignalStrength       uint16
	Jiffies              uint32
	OutputBufferSize     uint32
	OutputBufferFullness uint32
	ElapsedSeconds       uint32
	Voltage              uint16
	ElapsedMilliseconds  uint32
	ServerTimestamp      uint32
	ErrorCode            uint16
}

// Name returns the name of the STAT command
func (cmd *StatCommand) Name() string {
	return statCommandName
}

func (cmd *StatCommand) String() string {
	return ToJson(cmd)
}

func (cmd *StatCommand) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)

	binary.Write(buf, binary.BigEndian, []byte(statCommandName))
	binary.Write(buf, binary.BigEndian, uint32(statCommandSize))

	binary.Write(buf, binary.BigEndian, []byte(cmd.EventCode))
	binary.Write(buf, binary.BigEndian, cmd.NumCrLf)
	binary.Write(buf, binary.BigEndian, cmd.MasInitialized)
	binary.Write(buf, binary.BigEndian, cmd.MasMode)
	binary.Write(buf, binary.BigEndian, cmd.BufferSize)
	binary.Write(buf, binary.BigEndian, cmd.BufferFullness)
	binary.Write(buf, binary.BigEndian, cmd.BytesReceived)
	binary.Write(buf, binary.BigEndian, cmd.SignalStrength)
	binary.Write(buf, binary.BigEndian, cmd.Jiffies)
	binary.Write(buf, binary.BigEndian, cmd.OutputBufferSize)
	binary.Write(buf, binary.BigEndian, cmd.OutputBufferFullness)
	binary.Write(buf, binary.BigEndian, cmd.ElapsedSeconds)
	binary.Write(buf, binary.BigEndian, cmd.Voltage)
	binary.Write(buf, binary.BigEndian, cmd.ElapsedMilliseconds)
	binary.Write(buf, binary.BigEndian, cmd.ServerTimestamp)
	binary.Write(buf, binary.BigEndian, cmd.ErrorCode)

	return buf.Bytes(), nil
}
