package slimprotoclient

import "encoding/json"

// Size of the client command names (4 bytes)
const ClientCommandNameLengthByteSize = 4

// Size of the length information for client commands (4 bytes)
const ClientCommandDataLengthByteSize = 4

type ClientCommand interface {
	MarshalBinary() ([]byte, error)
	Name() string
	String() string
}

func ToJson(cmd ClientCommand) string {
	bytes, err := json.MarshalIndent(cmd, "", "  ")

	if err != nil {
		return err.Error()
	}

	return string(bytes)
}
