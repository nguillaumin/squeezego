package slimprotoclient

import (
	"bytes"
	"testing"

	"gitlab.com/nguillaumin/squeezego/pkg/slimproto"

	uuid "github.com/satori/go.uuid"
)

func TestMarshallBinaryHeloCommand(t *testing.T) {
	id := uuid.NewV4()
	capabilities := "cap1,cap2=x"

	cmd := HeloCommand{
		4,
		123,
		slimproto.MacAddress{1, 2, 3, 4, 5, 6},
		id,
		12345,
		9876543210,
		263,
		capabilities,
	}

	b, err := cmd.MarshalBinary()

	if err != nil {
		t.Error("Unexpected failure to marshall")
	}

	expectedLength := len("HELO") + ClientCommandDataLengthByteSize + 36 + len(capabilities)

	if len(b) != expectedLength {
		t.Errorf("Expected %v bytes but got %v", expectedLength, len(b))
	}

	expected := []byte{
		'H', 'E', 'L', 'O',
		0, 0, 0, 36 + byte(len(capabilities)),
		4,
		123,
		1, 2, 3, 4, 5, 6,
	}
	expected = append(expected, id.Bytes()...)
	expected = append(expected, []byte{
		48, 57,
		0, 0, 0, 2, 76, 176, 22, 234,
		1, 7,
		'c', 'a', 'p', '1', ',', 'c', 'a', 'p', '2', '=', 'x',
	}...)

	if !bytes.Equal(b, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, b)
	}
}
