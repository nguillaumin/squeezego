package slimprotoclient

import (
	"bytes"
	"encoding/binary"
)

const irCommandName = "IR  "
const irCommandSize = 10

const irCodeHeader = 0x7689

// IrCommand is the IR command sent by the client on infrared remote events
type IrCommand struct {
	Time   uint32
	Format byte
	NoBits byte
	IRCode uint16
}

// Name returns the name of the IR command
func (cmd *IrCommand) Name() string {
	return irCommandName
}

func (cmd *IrCommand) String() string {
	return ToJson(cmd)
}

// MarshalBinary marshalls the IR command as binary
func (cmd *IrCommand) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)

	binary.Write(buf, binary.BigEndian, []byte(irCommandName))
	binary.Write(buf, binary.BigEndian, uint32(irCommandSize))

	binary.Write(buf, binary.BigEndian, cmd.Time)
	binary.Write(buf, binary.BigEndian, cmd.Format)
	binary.Write(buf, binary.BigEndian, cmd.NoBits)
	binary.Write(buf, binary.BigEndian, uint16(irCodeHeader))
	binary.Write(buf, binary.BigEndian, cmd.IRCode)

	return buf.Bytes(), nil
}
