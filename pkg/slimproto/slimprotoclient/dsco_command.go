package slimprotoclient

import (
	"bytes"
	"encoding/binary"
)

const dscoCommandName = "DSCO"
const dscoCommandSize = 1

const (
	DscoClosedNormally  = 0
	DscoResetLocalHost  = 1
	DscoResetRemoteHost = 2
	DscoNoLongerWorking = 3
	DscoTimeout         = 4
)

type DscoCommand struct {
	Reason byte
}

// Name returns the name of the DSCO command
func (cmd *DscoCommand) Name() string {
	return dscoCommandName
}

func (cmd *DscoCommand) String() string {
	return ToJson(cmd)
}

// MarshalBinary marshalls the DSCO command as binary
func (cmd *DscoCommand) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)

	binary.Write(buf, binary.BigEndian, []byte(dscoCommandName))
	binary.Write(buf, binary.BigEndian, uint32(dscoCommandSize))

	binary.Write(buf, binary.BigEndian, cmd.Reason)

	return buf.Bytes(), nil
}
