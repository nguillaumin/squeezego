package slimprotoclient

import (
	"bytes"
	"encoding/binary"
)

const byeCommandName = "BYE!"
const byeCommandSize = 1

// ByeCommand is the BYE! command sent by the client on disconnection
type ByeCommand struct {
	Data byte
}

// Name returns the name of the BYE! command
func (cmd *ByeCommand) Name() string {
	return byeCommandName
}

func (cmd *ByeCommand) String() string {
	return ToJson(cmd)
}

// MarshalBinary marshalls the BYE! command as binary
func (cmd *ByeCommand) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)

	binary.Write(buf, binary.BigEndian, []byte(byeCommandName))
	binary.Write(buf, binary.BigEndian, uint32(byeCommandSize))
	binary.Write(buf, binary.BigEndian, cmd.Data)

	return buf.Bytes(), nil
}
