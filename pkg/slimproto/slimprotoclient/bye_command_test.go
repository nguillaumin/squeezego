package slimprotoclient

import (
	"bytes"
	"testing"
)

func TestMarshallBinaryByeCommand(t *testing.T) {
	cmd := ByeCommand{
		Data: 123,
	}

	b, err := cmd.MarshalBinary()

	if err != nil {
		t.Error("Unexpected failure to marshall")
	}

	expectedLength := len("BYE!") + ClientCommandDataLengthByteSize + 1

	if len(b) != expectedLength {
		t.Errorf("Expected %v bytes but got %v", expectedLength, len(b))
	}

	expected := []byte{
		'B', 'Y', 'E', '!',
		0, 0, 0, 1, 123,
	}

	if !bytes.Equal(b, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, b)
	}
}
