package slimprotoclient

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/nguillaumin/squeezego/pkg/slimproto"

	uuid "github.com/satori/go.uuid"
)

const heloCommandName = "HELO"
const heloCommandSize = 36

// DeviceID is the type of Squeezebox device
type DeviceID byte

const (
	_ DeviceID = iota
	_
	DeviceIDSqueezebox
	DeviceIDSoftSqueeze
	DeviceIDSqueezebox2
	DeviceIDTransporter
	DeviceIDSoftSqueeze3
	DeviceIDReceiver
	DeviceIDSqueezeSlave
	DeviceIDController
	DeviceIDBoom
	DeviceIDSoftBoom
	DeviceIDSqueezePlay
)

// HeloCommand is the HELO command sent by the client on login
type HeloCommand struct {
	DeviceID        DeviceID
	Revision        byte
	MAC             slimproto.MacAddress
	UUID            uuid.UUID
	WlanChannelList int16
	BytesReceived   uint64
	Language        uint16
	Capabilities    string
}

// Name returns the name of the HELO command
func (cmd *HeloCommand) Name() string {
	return heloCommandName
}

func (cmd *HeloCommand) String() string {
	return ToJson(cmd)
}

// MarshalBinary marshalls the HELO command as binary
func (cmd *HeloCommand) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)

	binary.Write(buf, binary.BigEndian, []byte(heloCommandName))
	binary.Write(buf, binary.BigEndian, uint32(heloCommandSize+len(cmd.Capabilities)))

	binary.Write(buf, binary.BigEndian, cmd.DeviceID)
	binary.Write(buf, binary.BigEndian, cmd.Revision)
	binary.Write(buf, binary.BigEndian, cmd.MAC)
	binary.Write(buf, binary.BigEndian, cmd.UUID.Bytes())
	binary.Write(buf, binary.BigEndian, cmd.WlanChannelList)
	binary.Write(buf, binary.BigEndian, cmd.BytesReceived)
	binary.Write(buf, binary.BigEndian, cmd.Language)

	binary.Write(buf, binary.BigEndian, []byte(cmd.Capabilities))

	return buf.Bytes(), nil
}
