package slimprotoclient

import (
	"bytes"
	"testing"
)

func TestMarshallBinaryIrCommand(t *testing.T) {

	cmd := IrCommand{
		1234567890,
		123,
		234,
		1234,
	}

	b, err := cmd.MarshalBinary()

	if err != nil {
		t.Error("Unexpected failure to marshall")
	}

	expectedLength := len("IR  ") + ClientCommandDataLengthByteSize + 10

	if len(b) != expectedLength {
		t.Errorf("Expected %v bytes but got %v", expectedLength, len(b))
	}

	expected := []byte{
		'I', 'R', ' ', ' ',
		0, 0, 0, irCommandSize,
		73, 150, 2, 210,
		123,
		234,
		118, 137, // IR code prefix
		4, 210,
	}

	if !bytes.Equal(b, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, b)
	}
}
