package slimprotoclient

import (
	"bytes"
	"testing"
)

func TestMarshallBinaryDscoCommand(t *testing.T) {

	cmd := DscoCommand{
		2,
	}

	b, err := cmd.MarshalBinary()

	if err != nil {
		t.Error("Unexpected failure to marshall")
	}

	expectedLength := len("DSCO") + ClientCommandDataLengthByteSize + 1

	if len(b) != expectedLength {
		t.Errorf("Expected %v bytes but got %v", expectedLength, len(b))
	}

	expected := []byte{
		'D', 'S', 'C', 'O',
		0, 0, 0, 1,
		2,
	}

	if !bytes.Equal(b, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, b)
	}
}
