package slimprotoserver

import (
	"encoding/binary"
	"fmt"
	"net"
)

const strmCommandName = "strm"
const strmMinCommandSize = 24

const (
	StrmCommandStart     = "s"
	StrmCommandPause     = "p"
	StrmCommandUnpause   = "u"
	StrmCommandStop      = "q"
	StrmCommandStatus    = "t"
	StrmCommandFlush     = "f"
	StrmCommandSkipAhead = "a"
)

type replayGain [2]uint16

type StrmCommand struct {
	Command          string
	Autostart        string
	FormatByte       string
	PCMSampleSize    string
	PCMSampleRate    string
	PCMChannels      string
	PCMEndian        string
	Threshold        byte
	SPDIFEnable      string
	TransitionPeriod byte
	TransitionType   string
	Flags            byte
	OutputThreshold  byte
	Reserved         byte
	ReplayGain       replayGain
	ServerPort       uint16
	ServerIP         net.IP
	HTTPRequest      string
}

func (cmd *StrmCommand) String() string {
	return ToJson(cmd)
}

func (cmd *StrmCommand) UnmarshalBinary(data []byte) error {
	if len(data) < strmMinCommandSize {
		return fmt.Errorf("Invalid length, expected at least %v bytes but got %v", strmMinCommandSize, len(data))
	}

	cmd.Command = string(data[0])
	cmd.Autostart = string(data[1])
	cmd.FormatByte = string(data[2])
	cmd.PCMSampleSize = string(data[3])
	cmd.PCMSampleRate = string(data[4])
	cmd.PCMChannels = string(data[5])
	cmd.PCMEndian = string(data[6])
	cmd.Threshold = data[7]
	cmd.SPDIFEnable = string(data[8])
	cmd.TransitionPeriod = data[9]
	cmd.TransitionType = string(data[10])
	cmd.Flags = data[11]
	cmd.OutputThreshold = data[12]
	cmd.Reserved = data[13]
	cmd.ReplayGain = replayGain{binary.BigEndian.Uint16(data[14:16]), binary.BigEndian.Uint16(data[16:18])}
	cmd.ServerPort = binary.BigEndian.Uint16(data[18:20])
	cmd.ServerIP = net.IPv4(data[20], data[21], data[22], data[23])

	if len(data) > strmMinCommandSize {
		// Subsequent bytes may be an HTTP request string
		cmd.HTTPRequest = string(data[strmMinCommandSize:])
	}

	return nil
}

func (cmd *StrmCommand) ReplayGainAsTimestamp() uint32 {
	return uint32(uint32(cmd.ReplayGain[0])<<16 | uint32(cmd.ReplayGain[1]))
}
