package slimprotoserver

import (
	"fmt"
)

const versCommandName = "vfdc"
const versMinCommandSize = 1

type VersCommand struct {
	Version string
}

func (cmd *VersCommand) String() string {
	return ToJson(cmd)
}

func (cmd *VersCommand) UnmarshalBinary(data []byte) error {
	if len(data) < versMinCommandSize {
		return fmt.Errorf("Invalid length, expected at least %v bytes but got %v", vfdcMinCommandSize, len(data))
	}

	cmd.Version = string(data)
	return nil
}
