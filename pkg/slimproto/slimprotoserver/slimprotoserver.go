package slimprotoserver

import "encoding/json"

// Size of the server command names (4 bytes)
const ServerCommandNameLengthByteSize = 4

// Size of the length information for server commands (2 bytes)
const ServerCommandDataLengthByteSize = 2

type ServerCommand interface {
	UnmarshalBinary(data []byte) error
	String() string
}

func ToJson(cmd ServerCommand) string {
	bytes, err := json.MarshalIndent(cmd, "", "  ")

	if err != nil {
		return err.Error()
	}

	return string(bytes)
}
