package slimprotoserver

import (
	"fmt"
)

const vfdcCommandName = "vfdc"
const vfdcMinCommandSize = 168

type VfdcCommand struct {
	VfdcData []byte
}

func (cmd *VfdcCommand) String() string {
	return ToJson(cmd)
}

func (cmd *VfdcCommand) UnmarshalBinary(data []byte) error {
	if len(data) < vfdcMinCommandSize {
		return fmt.Errorf("Invalid length, expected at least %v bytes but got %v", vfdcMinCommandSize, len(data))
	}

	cmd.VfdcData = data
	return nil
}
