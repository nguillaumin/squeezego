package slimprotoserver

import (
	"encoding/binary"
	"fmt"
)

const audgCommandName = "audg"
const audgMinCommandSize = 18

type newLevel [2]uint16

type AudgCommand struct {
	OldLeft              uint32
	OldRight             uint32
	DigitalVolumeControl byte
	Preamp               byte
	NewLeft              newLevel
	NewRight             newLevel
	Sequence             uint32
}

func (cmd *AudgCommand) String() string {
	return ToJson(cmd)
}

func (cmd *AudgCommand) UnmarshalBinary(data []byte) error {
	if len(data) < audgMinCommandSize {
		return fmt.Errorf("Invalid length, expected at least %v bytes but got %v", audgMinCommandSize, len(data))
	}

	cmd.OldLeft = binary.BigEndian.Uint32(data[0:4])
	cmd.OldRight = binary.BigEndian.Uint32(data[4:8])
	cmd.DigitalVolumeControl = data[8]
	cmd.Preamp = data[9]
	cmd.NewLeft = newLevel{binary.BigEndian.Uint16(data[10:12]), binary.BigEndian.Uint16(data[12:14])}
	cmd.NewRight = newLevel{binary.BigEndian.Uint16(data[14:16]), binary.BigEndian.Uint16(data[16:18])}
	if len(data) > audgMinCommandSize {
		cmd.Sequence = binary.BigEndian.Uint32(data[18:22])
	}

	return nil
}
