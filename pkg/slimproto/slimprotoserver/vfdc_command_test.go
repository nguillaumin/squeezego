package slimprotoserver

import (
	"reflect"
	"testing"
)

func TestUnmarshallBinaryVfdcCommand(t *testing.T) {

	b := make([]byte, 168)
	for i := 0; i < len(b); i++ {
		b[i] = byte(i)
	}

	expected := VfdcCommand{
		b,
	}

	cmd := VfdcCommand{}

	err := cmd.UnmarshalBinary(b)

	if err != nil {
		t.Error("Unexpected failure to unmarshall", err)
	}

	if !reflect.DeepEqual(cmd, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, cmd)
	}

}
