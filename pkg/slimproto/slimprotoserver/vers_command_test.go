package slimprotoserver

import (
	"reflect"
	"testing"
)

func TestUnmarshallBinaryVersCommand(t *testing.T) {

	b := []byte{
		'1', '.', '2', '.', '3',
	}

	expected := VersCommand{
		"1.2.3",
	}

	cmd := VersCommand{}

	err := cmd.UnmarshalBinary(b)

	if err != nil {
		t.Error("Unexpected failure to unmarshall", err)
	}

	if !reflect.DeepEqual(cmd, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, cmd)
	}

}
