package slimprotoserver

import (
	"net"
	"reflect"
	"testing"
)

func TestUnmarshallBinaryStrmCommand(t *testing.T) {

	expected := StrmCommand{
		"q",
		"1",
		"m",
		"2",
		"3",
		"?",
		"1",
		123,
		"1",
		94,
		"3",
		112,
		37,
		0,
		replayGain{65530, 12345},
		9123,
		net.ParseIP("192.168.1.12"),
		"GET /stream.mp3?player=$client-id HTTP/1.0\r\n\r\n",
	}

	b := []byte{
		'q',
		'1',
		'm',
		'2',
		'3',
		'?',
		'1',
		123,
		'1',
		94,
		'3',
		112,
		37,
		0,
		255, 250, 48, 57,
		35, 163,
		192, 168, 1, 12,
	}
	b = append(b, []byte("GET /stream.mp3?player=$client-id HTTP/1.0\r\n\r\n")...)

	cmd := StrmCommand{}

	err := cmd.UnmarshalBinary(b)

	if err != nil {
		t.Error("Unexpected failure to unmarshall", err)
	}

	if !reflect.DeepEqual(cmd, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, cmd)
	}
}

func TestStrmCommandReplayGainAsTimestamp(t *testing.T) {
	strm := StrmCommand{}
	strm.ReplayGain = replayGain{123, 123}

	expected := uint32(8061051)
	ts := strm.ReplayGainAsTimestamp()

	if ts != expected {
		t.Errorf("Expected %v but got %v", expected, ts)
	}
}
