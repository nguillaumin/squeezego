package slimprotoserver

import (
	"reflect"
	"testing"
)

func TestUnmarshallBinaryAudgCommand(t *testing.T) {

	expected := AudgCommand{
		16843009,
		33686018,
		1,
		127,
		newLevel{771, 1028},
		newLevel{1285, 1542},
		117901063,
	}

	b := []byte{
		1, 1, 1, 1,
		2, 2, 2, 2,
		1,
		127,
		3, 3, 4, 4,
		5, 5, 6, 6,
		7, 7, 7, 7,
	}

	cmd := AudgCommand{}

	err := cmd.UnmarshalBinary(b)

	if err != nil {
		t.Error("Unexpected failure to unmarshall", err)
	}

	if !reflect.DeepEqual(cmd, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, cmd)
	}
}

func TestUnmarshallBinaryAudgCommandNoSequence(t *testing.T) {

	expected := AudgCommand{
		16843009,
		33686018,
		1,
		127,
		newLevel{771, 1028},
		newLevel{1285, 1542},
		0,
	}

	b := []byte{
		1, 1, 1, 1,
		2, 2, 2, 2,
		1,
		127,
		3, 3, 4, 4,
		5, 5, 6, 6,
	}

	cmd := AudgCommand{}

	err := cmd.UnmarshalBinary(b)

	if err != nil {
		t.Error("Unexpected failure to unmarshall", err)
	}

	if !reflect.DeepEqual(cmd, expected) {
		t.Errorf("Expected:\n %v but got:\n %v", expected, cmd)
	}
}
