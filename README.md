# SqueezeGo

A [Squeezebox](https://en.wikipedia.org/wiki/Squeezebox_(network_music_player))
client written in Go, inspired by
[Squeezelite](https://github.com/ralph-irving/squeezelite)

This is a work in progress. The basics are working, but the implementation of
playing streams is pretty naive, and a lot of features are missing. See the
[protocol implementation status](docs/protocol-status.md) for details.

The end goal is to have a self-contained binary, easy to deploy, that handles
advanced features like infrared remote and LCD display. Ultimately it should be
able to completely replace a hardware Squeezebox.>
