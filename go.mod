module gitlab.com/nguillaumin/squeezego

go 1.14

require (
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/gorilla/websocket v1.4.2
	github.com/hajimehoshi/go-mp3 v0.2.1
	github.com/jfreymuth/oggvorbis v1.0.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mewkiz/flac v1.0.6
	github.com/satori/go.uuid v1.2.0
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/sirupsen/logrus v1.5.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
