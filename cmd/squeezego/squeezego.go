package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient"
	"gitlab.com/nguillaumin/squeezego/pkg/squeezeclient/log"

	"github.com/gordonklaus/portaudio"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&log.FilteringFormatter{
		ParentFormatter: &logrus.TextFormatter{
			FullTimestamp: true,
		},
		Levels: map[string]logrus.Level{
			"client.slimproto": logrus.WarnLevel,
			"client.command":   logrus.InfoLevel,
		},
	})

	err := portaudio.Initialize()
	if err != nil {
		fmt.Printf("Error initializing PortAudio: %v", err)
		return
	}
	defer portaudio.Terminate()

	defaultDevice, err := portaudio.DefaultOutputDevice()
	if err != nil {
		fmt.Printf("Error getting default audio device: %v", err)
		return
	}

	server := flag.String("server", "", "Slimserver hostname")
	port := flag.Uint("port", 3483, "Slimserver port")
	listAudioDevices := flag.Bool("list-devices", false, "List audio devices")
	audioDeviceName := flag.String("device", (*defaultDevice).Name, "Audio device name")
	webcontrolPort := flag.Uint("webcontrol-port", 0, "Web control port. 0 to disable the web control interface")

	flag.Parse()

	if *listAudioDevices {
		devices, err := portaudio.Devices()
		if err != nil {
			fmt.Printf("Error listing audio devices: %v", err)
		}
		fmt.Printf("List of audio devices:\n")
		for _, device := range devices {
			fmt.Printf("\t%v: %v (%v output channels)\n", device.HostApi.Name, device.Name, device.MaxOutputChannels)
		}
		return
	}

	var audioDevice *portaudio.DeviceInfo
	devices, err := portaudio.Devices()
	if err != nil {
		fmt.Printf("Error listing audio devices: %v", err)
		return
	}
	for _, device := range devices {
		if device.Name == *audioDeviceName {
			audioDevice = device
		}
	}

	if *server == "" {
		flag.Usage()
		return
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	log.GetLog("cli").Infof("Connecting to %v:%v", *server, *port)
	client := squeezeclient.NewSqueezeClient(*server, uint16(*port), audioDevice, uint16(*webcontrolPort))

	client.Connect()

	go client.Start()

	select {
	case <-sigs:
		log.GetLog("cli").Infof("Signal caught, closing client")
		client.Close()
	}
}
